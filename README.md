# README #

In many enterprise systems, we tend to recieve flat files with data to be loaded in the internal database, especially in a data warehouse. There are often cases where the data may not be of expected type or format which may lead to job failures, DB load issue or data corruption.
Any such issues may further require the system to be cleaned up before the file can be corrected and re-run, and may involve additional effort.

File-Lens is a simple yet powerful file validation application that can be used to scan through a given ASCII flat file and validate the data based on the user defined config file.

By using this application beforehand, the user would be able to decide whether the given file is acceptable or requires any correction even before loading into the internal systems.

### Validations ###

File-Lens is capable of handling the following validations on a given file. All the below validations can be controlled using a config file.

* File level validation
** File name
** File encoding

* Header level validation
** Header availability
** Header name
** Header count

* Field level validation
* Data type
* Data format
* Length constraints

Note : See the config template available inside the *./config/template* directory for more details on the validations available.


### System Requirements ###

* Python version : 3.6.x
* OS supported: Windows/ Linux/ Mac 64 bit (recommended)

### How do I get set up? ###

* Download the source files
* Create a config file by referring to the template that is available inside the *./config/template* directory
* Bring up the cmd / terminal window and run


```
#!python

python3 app.py --config "/path/to/config_file" --test "/path/to/test_file"
```


* The validation results are now displayed in the terminal

### Options ###

When validating a very large file that is in GB, you may not want all the cpu resource to be consumed by this process. By specifying an additional flag *--nice*, the application is set to run in low priority mode therefore you would be able to run other applications in parallel without any lags.


```
#!python

python3 app.py --config "/path/to/config_file" --test "/path/to/test_file" --nice
```


### Who do I talk to? ###

This project is part of crazy-brains project.
For any issues, please write directly to the developer - *Ram Prakash Jayapalan* <ramp16888@gmail.com>
