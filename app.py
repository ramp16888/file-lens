#!/usr/bin/env python3

import argparse
from modules.main import Main
import sys

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


class App(object):

    def __init__(self, configfile, testfile, nice):
        """
        Constructor
        :param configfile: <string> Full path to the config file
        :param testfile: <string> Full path to the test file
        """
        self.configfile = configfile
        self.testfile = testfile
        self.nice = nice

    def validate(self):
        """
        Function to validate the test file
        :return: <dict> result
        """
        result = Main(self.configfile, self.testfile, self.nice).validate()
        return result


if __name__ == "__main__":

    try:

        parser = argparse.ArgumentParser()
        parser.add_argument("--config", "-c")
        parser.add_argument("--test", "-t")
        parser.add_argument("--nice", action="store_true")

        args = parser.parse_args()

        configfile = args.config
        testfile = args.test
        nice = args.nice

        app = App(configfile, testfile, nice).validate()

        print(app)
        sys.exit(0)

    except Exception as ex:

        print(ex)
        sys.exit(1)

