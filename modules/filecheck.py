#!/usr/bin/env python3

# This module handles all the file level checks ie., filename and file encoding

import ntpath
import helpers.isexpectedformat as isexpectedformat
import helpers.rawencoding as rawencoding

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


class FileCheck(object):

    def __init__(self, config, testfile):
        """
        Constructor
        :param config: <object> of type config
        :param testfile: Full path to the test file
        """
        if config is None:
            raise FileNotFoundError("Config file not found.")
        if testfile is None:
            raise FileNotFoundError("Test file not found.")
        self.config = config
        self.testfile = testfile

    def validate(self):
        """
        Call this function to validate on the various file level checks
        :return: <dict> containing result
        """
        result = {"filecheck": {"filename": {"isvalid": True, "errormsg": ""},
                                "encoding": {"isvalid": True, "errormsg": ""}}}

        # Validate filename if set to true in the config file
        if "Filename" in self.config["file.rules"]:
            filepath, filename = ntpath.split(self.testfile.replace("\\", "/"))
            if not isexpectedformat.validate(self.config["file.rules"]["Filename"], filename):
                result["filecheck"]["filename"]["isvalid"] = False
                if "FilenameErrorMessage" in self.config["file.rules"]:
                    result["filecheck"]["filename"]["errormsg"] = self.config["file.rules"]["FilenameErrorMessage"]
                else:
                    result["filecheck"]["filename"]["errormsg"] = "The given test file did not meet the expected " \
                                                                  "filename or format."

        # Validate file encoding if set to true in the config file
        if "Encoding" in self.config["file.rules"]:
            enc = rawencoding.getfromfile(self.testfile)
            v2 = False

            # Let's also double check the file encoding by opening the file in the expected format in binary mode
            # validate using codecs
            with open(self.testfile, "rb") as f:
                lines = f.read(50000)
                try:
                    lines.decode(self.config["file.rules"]["Encoding"])
                    v2 = True
                except UnicodeError:
                    pass

            # Check if either of the above validations (using chardet or codecs) is True
            if (enc != self.config["file.rules"]["Encoding"]) & (v2 is not True):
                result["filecheck"]["encoding"]["isvalid"] = False
                if "EncodingErrorMessage" in self.config["file.rules"]:
                    result["filecheck"]["encoding"]["errormsg"] = self.config["file.rules"]["EncodingErrorMessage"]
                else:
                    result["filecheck"]["encoding"]["errormsg"] = "The given test file did not meet the expected " \
                                                                  "encoding standard."
        return result

if __name__ == "__main__":
    import modules.configfileparser as parser
    import os
    configfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "config", "sample.ini"))
    testfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "sample_files", "NSC_sample_20170819.txt"))
    config = parser.parse(configfile)
    res = FileCheck(config, testfile).validate()
    print(res)

