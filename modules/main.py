#!/usr/bin/env python3

import os
import modules.configfileparser as configfileparser
import time
from modules.filecheck import FileCheck
from modules.headercheck import HeaderCheck
from modules.fieldcheck import FieldCheck

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


class Main(object):

    VALID_FILE_EXT = [".txt", ".dat", ".csv", ".log"]

    def __init__(self, configfile, testfile, nice=False):
        """
        Class constructor
        :param configfile: <str> Full path to the config file
        :param testfile: <str> Full path to the test file
        :param nice: <bool> If set to True the script executes in low priority mode thus easing on the cpu overhead.
        This may require super user or admin privileges.
        """
        self.configfile = configfile

        if not os.path.isfile(testfile):
            raise FileNotFoundError("The given file {0} is not a valid file path".format(testfile))
        filename, filext = os.path.splitext(testfile)
        if filext.lower() not in Main.VALID_FILE_EXT:
            raise TypeError("The given file format is not supported. Allowed file "
                            "formats {0}".format(Main.VALID_FILE_EXT))
        self.testfile = testfile
        self.nice = nice

    def validate(self):
        """
        Function to validate various checks on the file
        :return: <str> validation result
        """
        # Get the config object
        config = configfileparser.parse(self.configfile)

        # Perform file level checks. If error, do not proceed with header and field level checks
        filecheck = FileCheck(config, self.testfile).validate()
        if (filecheck["filecheck"]["filename"]["isvalid"] is False) | (filecheck["filecheck"]["encoding"]["isvalid"]
                                                                       is False):
            return filecheck

        # Perform header level checks. If error, do not proceed with field level checks
        headercheck = HeaderCheck(config, self.testfile).validate()
        if (headercheck["headercheck"]["headername"]["isvalid"] is False) | \
            (headercheck["headercheck"]["headercount"]["isvalid"] is False):
            return headercheck

        # Perform field level checks
        fieldcheck = FieldCheck(config, self.testfile, self.nice).validate()

        return fieldcheck


# Main function call
if __name__ == "__main__":
    configfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "config",
                                              "NSC_Vehicle_Recall.ini"))
    testfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "sample_files",
                                            "NSC_VehicleRecall_20160621.txt"))
    starttime = time.time()
    result = Main(configfile, testfile, False).validate()
    endtime = (time.time() - starttime) // 60
    print(result)
    print("Run time : {0}".format(endtime))







