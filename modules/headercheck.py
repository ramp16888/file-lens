#!/usr/bin/env python3

# This module validates the header from the test file if applicable. It can validates on the header availability,
# header name and header count

import helpers.delimiterparser as delimiterparser

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


class HeaderCheck(object):

    def __init__(self, config, testfile):
        """
        Constructor
        :param config: <object> of type config
        :param testfile: Full path to the test file
        """
        if config is None:
            raise FileNotFoundError("Config file not found.")
        if testfile is None:
            raise FileNotFoundError("Test file not found.")
        self.config = config
        self.testfile = testfile

    def validate(self):
        """
        Function that validates on header in the given file
        :return: <dict> containing result
        """
        result = {"headercheck": {"containsheader": True, "headername": {"isvalid": True, "errormsg": []},
                                  "headercount": {"isvalid": True, "errormsg": ""}}}

        # If the file is not expected to contain header, we need not proceed with any of the header checks.
        # We simply mark the contains header key value to false and return
        if not self.config["header.rules"].getboolean("ContainsHeader"):
            result["headercheck"]["containsheader"] = False
            return result

        # Validate header only if set to true
        if not self.config["header.rules"].getboolean("ValidateHeader"):
            return result

        availableheaders = []
        with open(self.testfile, mode="r", encoding="utf-8") as f:
            line = f.readline()
            availableheaders = delimiterparser.getdelimitedstring(line, self.config["global.settings"]["Delimiter"])

        casesensitive = self.config["header.rules"].getboolean("CaseSensitive")

        givenheaders = [x for x in self.config["header"]] if casesensitive \
            else [x.lower() for x in self.config["header"]]
        availableheaders = availableheaders if casesensitive else [x.lower() for x in availableheaders]

        # Check for the header names in the given file
        for each in givenheaders:
            if each not in availableheaders:
                errormsg = self.config["header"]["HeaderErrorMessage"] if "HeaderErrorMessage" in self.config["header"] \
                    else "Could not find a match for the header '{0}'".format(each)
                errormsg = errormsg.replace("{headername}", each)
                result["headercheck"]["headername"]["errormsg"].append(errormsg)
            if len(result["headercheck"]["headername"]["errormsg"]) > 0:
                result["headercheck"]["headername"]["isvalid"] = False

        # Validate on the header count only if set to true, else return
        if not self.config["header.rules"].getboolean("MatchHeaderCount"):
            return result

        # Check for the header count
        if len(givenheaders) != len(availableheaders):
            result["headercheck"]["headercount"]["isvalid"] = False
            errormsg = self.config["header"]["HeaderCountErrorMessage"] if "HeaderCountErrorMessage" in \
                self.config["header"] else "Header count does not match (required : {0} | " \
                "available : {1})".format(len(givenheaders), len(availableheaders))
            errormsg = errormsg.replace("{requiredcount}", str(len(givenheaders))).replace("{availablecount}",
                                                                                           str(len(availableheaders)))
            result["headercheck"]["headercount"]["errormsg"] = errormsg

        return result

if __name__ == "__main__":
    import modules.configfileparser as parser
    import os
    configfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "config", "sample.ini"))
    testfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "sample_files", "NSC_sample_20170819.txt"))
    config = parser.parse(configfile)
    res = HeaderCheck(config, testfile).validate()
    print(res)
