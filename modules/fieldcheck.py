#!/usr/bin/env python3

# This module performs the field level checks on the given file

import helpers.isexpectedformat as isexpectedformat
import helpers.delimiterparser as delimiterparser
import helpers.isexpectedlength as isexpectedlength
import helpers.isalphanumeric as isalphanumeric
import os
import multiprocessing
from functools import partial
import helpers.isnumeric as isnumeric
import json
import ast
import sys
import threading

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


class FieldCheck(object):

    def __init__(self, config, testfile, nice=False):
        """
        Constructor
        :param config: <object> of type config
        :param testfile: Full file path to the test file
        :param nice: Go nice on the multiprocess
        """
        if config is None:
            raise FileNotFoundError("Config file not found.")
        if testfile is None:
            raise FileNotFoundError("Test file not found.")
        self.config = config
        self.testfile = testfile
        self.nice = nice

    def validate(self):
        """
        Call this function to perform the field level validation
        :return: <dict> containing result
        """
        result = {"fieldcheck": {"isvalid": True, "errors": {}}}

        # Return the result if the fields need not be validated. Else proceed
        if not self.config["column.rules"].getboolean("ValidateColumn"):
            return result

        # Get all the checks from the config file and map the headers accordingly
        checks = [x for x in self.config.sections() if isexpectedformat.validate("\w+Check$", x)]
        checklist = dict()
        for each in checks:
            checklist[each] = [x.lower() for x in self.config[each]]

        # Set the chunk size based on the size of the file
        filesize = os.path.getsize(self.testfile)   # In bytes

        if 0 >= filesize <= 100000:
            chunksize = 10
        elif 100001 >= filesize <= 1000000:
            chunksize = 100
        else:
            chunksize = 1000

        # Initialize a multiprocessing pool manager to carry out the validation task
        pool = multiprocessing.Pool()

        # Initialize a queue manager and a queue to share results between process
        queue = multiprocessing.Manager().Queue()

        # Initialize a queue manager and a queue to hold the aggregate result from the consumer process
        resultqueue = multiprocessing.Manager().Queue()

        # Create a consumer process that reads the queue and performs aggregation
        consumerprocess = threading.Thread(target=readfromqueue, args=(queue, resultqueue))
        consumerprocess.daemon = True
        consumerprocess.start()

        try:

            # Skip the 1st line if header is expected, else read all the lines.
            # If header is not expected, assign arbitrary header names of the pattern "Column1, Column2,..."
            with open(self.testfile, mode="r", encoding="utf-8") as f:

                availableheader = delimiterparser.getdelimitedstring(f.readline(),
                                                                     self.config["global.settings"]["Delimiter"])
                if not self.config["header.rules"].getboolean("ContainsHeader"):
                    availableheader = ["Column{0}".format(i+1) for i in range(0, len(availableheader))]
                    f.seek(0)

                # Span the file buffer across pool of workers
                pool.map(partial(runcheck, availableheader, checklist, self.config, queue, self.nice), f, chunksize)

        finally:

            # Close the pool
            pool.close()
            # Join the pool
            pool.join()
            # Once the pool process is complete, put <None> to the queue to terminate the consumer process
            queue.put(None)
            # Join on the queue, since the consumer process is a daemon process
            queue.join()

        if resultqueue.qsize() > 0:
            result = resultqueue.get()
            resultqueue.task_done()

        return result


def readfromqueue(queue, resultqueue):
    """
    Function to read from queue and perform aggregation
    :param queue: <Queue> object
    :return: None
    """
    res = {"fieldcheck": {"isvalid": True, "errors": {}}}

    while True:
        # Read from the queue
        read = queue.get()
        # If no task is available, exit the iterator
        if read is None:
            queue.task_done()
            break
        # Mark the current task is done
        queue.task_done()
        # Set a flag to track invalid records
        isinvalid  = False
        # Iterate through result and aggregate
        for check, headers in read.items():
            if not headers:
                continue
            isinvalid = True
            if check not in res["fieldcheck"]["errors"]:
                res["fieldcheck"]["errors"][check] = dict()
            for header, errorcount in headers.items():
                res["fieldcheck"]["errors"][check][header] = errorcount \
                    if header not in res["fieldcheck"]["errors"][check] else \
                    res["fieldcheck"]["errors"][check][header] + errorcount

        res["fieldcheck"]["totalrecords"] = 1 if "totalrecords" not in res["fieldcheck"] else \
            res["fieldcheck"]["totalrecords"] + 1
        if isinvalid:
            res["fieldcheck"]["isvalid"] = False
            res["fieldcheck"]["invalidrecords"] = 1 if "invalidrecords" not in res["fieldcheck"] else \
                res["fieldcheck"]["invalidrecords"] + 1

    resultqueue.put(res)


# Function that runs various checks on the given lines from a file
def runcheck(availableheader, checklist, config, queue, nice, li):
    """
    Function that runs various checks on the given line from the file
    :param checklist: <dict> with various checks mapped to the headers
    :param availableheader: <list> header from the file
    :param config: <object> config object
    :param li: <string> line read from file
    :return: None
    """
    # Check if niceness needs to attached to the process to consume less CPU
    if nice:
        lowpriority()

    res = dict()

    line = delimiterparser.getdelimitedstring(li, config["global.settings"]["Delimiter"])

    # Map header and header values in a dict object
    di = dict()
    for i in range(0, len(availableheader)):
        di[availableheader[i].lower()] = line[i]

    for check, headers in checklist.items():

        for header in headers:

            if header not in di:
                continue

            if check not in res:
                res[check] = dict()

            isempty = True if len(di[header].strip()) == 0 else False
            isnull = True if di[header].lower() == "null" else False

            # Blank/empty check
            if (check == "EmptyCheck") & (isempty):
                res[check][header] = 1

            # Null check
            elif (check == "NullCheck") & (isnull):
                res[check][header] = 1

            # Numeric check
            elif (check == "NumericCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_numeric(di[header]):
                    res[check][header] = 1

            # Integer check
            elif (check == "IntegerCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_integer(di[header]):
                    res[check][header] = 1

            # Signed integer check
            elif (check == "SignedIntegerCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_signed_integer(di[header]):
                    res[check][header] = 1

            # Unsigned integer check
            elif (check == "UnSignedIntegerCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_unsigned_integer(di[header]):
                    res[check][header] = 1

            # Decimal check
            elif (check == "DecimalCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_decimal(di[header]):
                    res[check][header] = 1

            # Signed decimal check
            if (check == "SignedDecimalCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_signed_decimal(di[header]):
                    res[check][header] = 1

            # Unsigned decimal check
            elif (check == "UnSignedDecimalCheck") & (isempty is False) & (isnull is False):
                if not isnumeric.is_unsigned_decimal(di[header]):
                    res[check][header] = 1

            # Format check
            elif (check == "FormatCheck") & (isempty is False) & (isnull is False):
                # Get the format params from the config file
                params = [config["FormatCheck"][x] for x in config["FormatCheck"] if x.lower() == header]
                # Try converting to a dict object
                try:
                    val = json.loads(params[0])
                except json.JSONDecodeError:
                    val = ast.literal_eval(params[0])
                if ("pattern" in val) & ("count" in val):
                    if isexpectedformat.count(val["pattern"], di[header]) != val["count"]:
                        res[check][header] = 1
                elif ("pattern" in val) & ("count" not in val):
                    if not isexpectedformat.validate(val["pattern"], di[header]):
                        res[check][header] = 1

            # Length check
            elif (check == "LengthCheck") & (isempty is False) & (isnull is False):
                # Get the format params from the config file
                params = [config["LengthCheck"][x] for x in config["LengthCheck"] if x.lower() == header]
                # Try converting to a dict object
                try:
                    val = json.loads(params[0])
                except json.JSONDecodeError:
                    val = ast.literal_eval(params[0])
                if ("min" in val) & ("max" in val):
                    if not isexpectedlength.validate(di[header], val["min"], val["max"]):
                        res[check][header] = 1
                elif ("min" in val) & ("max" not in val):
                    if not isexpectedlength.validate(di[header], val["min"]):
                        res[check][header] = 1

            # Alphanumeric check
            elif (check == "AlphaNumericCheck") & (isempty is False) & (isnull is False):
                if not isalphanumeric.validate(di[header]):
                    res[check][header] = 1

    queue.put_nowait(res)


def lowpriority():
    """
    Set the priority of the multiprocess below normal
    :return: None
    """
    try:
        if sys.platform.lower().startswith("win"):
            iswindows = True
        else:
            iswindows = False

    except Exception:
        return None

    if iswindows:
        # Based on:
        #   "Recipe 496767: Set Process Priority In Windows" on ActiveState
        #   http://code.activestate.com/recipes/496767/
        import win32api, win32process, win32con

        pid = win32api.GetCurrentProcessId()
        handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
        win32process.SetPriorityClass(handle, win32process.BELOW_NORMAL_PRIORITY_CLASS)

    else:
        try:
            niceval = os.nice(-1)
            while niceval > 1:
                n = os.nice(-1)
                if n != 1:
                    continue
                else:
                    break
        except PermissionError:
            raise PermissionError("The user does not have the right privileges to set the process to low priority.")
        print(n)


if __name__ == "__main__":
    import modules.configfileparser as parser
    import time
    configfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "config",
                                              "NSC_Vehicle_Recall.ini"))
    testfile = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "sample_files",
                                            "NSC_Vehicle_Recall_20170501.txt"))
    config = parser.parse(configfile)
    start = time.time()
    res = FieldCheck(config, testfile).validate()
    end = (time.time() - start)//60
    print(str(end))
