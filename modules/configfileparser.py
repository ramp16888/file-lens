#!/usr/bin/env python3

# This module parses the given config file and returns the config object

import configparser
import os

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


def parse(configfile):
    """
    Parses the config file and creates the config object
    :param configfile: Full file path to the config file
    :return: <object> of type config
    """
    config = configparser.ConfigParser(allow_no_value=True)
    # Read all the key:value pairs in the config file as-is. By default, config parser converts to lower case
    config.optionxform = str

    # Check if the given file is a valid file and has a proper .ini extension
    if not os.path.isfile(configfile):
        raise FileNotFoundError("The given file path {0} is not a valid file path.".format(configfile))
    filename, filext = os.path.splitext(configfile)
    if filext.lower() != ".ini":
        raise FileNotFoundError("The given file {0} is not a valid config file.".format(configfile))

    config.read(configfile)
    return config


if __name__ == "__main__":
    configfile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', "config", "sample.ini"))
    config = parse(configfile)
    print(config.sections())
