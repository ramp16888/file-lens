#!/usr/bin/env python3

# This module returns True if the given value meets the given length constraints else False.
# The length() function can be used to get the length of the input string.

import helpers.isnumeric as isnumeric

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


def validate(value, minlen, maxlen=None):

    # Max value is optional. If max value is not provided, then the validation is done only on the min value
    # check if the min and the max value(if available) are integers

    try:

        # Check if the given min value is unsigned integer
        if isnumeric.is_unsigned_integer(minlen) is False:
            raise TypeError("The given min value " + str(minlen) + " is not an unsigned integer.")

        # Check if the max length is provided
        if maxlen is not None:

            # Check if the given max value is unsigned integer
            if isnumeric.is_unsigned_integer(maxlen) is False:
                raise TypeError("The given max value " + str(maxlen) + " is not an unsigned integer.")

            # if len(value) >= int(minlen) and len(value) <= int(maxlen):
            if int(minlen) <= len(value) <= int(maxlen):
                return True
            else:
                return False

        # If no max value available, validate only on the min value
        if len(value) >= int(minlen):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError(str(vErr))

    except TypeError as tErr:
        raise TypeError("The given min/max value cannot be converted to an integer. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))

