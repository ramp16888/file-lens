#!/usr/bin/env python3

# This helper module accepts a delimited string and the delimiter as params and returns a py list containing the split
# strings.

import re

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


def getdelimitedstring(string, delimiter):

    try:
        # Trim the delimiter and new line chars from the end of the line, and split on delimiter
        li = re.split(r"{0}".format(delimiter + "{1}"), string.rstrip(delimiter).rstrip("\r\n"))
        # Check if there are any non empty value in the above delimiter splitted Py list. If not consider it as an
        # empty list
        if len(list(filter(len, li))) == 0:
            li = []

        return li

    except ValueError as vErr:
        raise ValueError(
            "The given string " + string + " cannot be parsed using the delimiter " + delimiter + ". Exception : "
            + str(
                vErr))

    except Exception as ex:
        raise Exception(str(ex))
