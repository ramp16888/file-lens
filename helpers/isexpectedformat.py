#!/usr/bin/env python3

# This module validates whether the given value is of expected format.

import re

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


def validate(pattern, value):
    """
    Returns true if the value matches the given regex pattern, else false.
    :param pattern: Regex pattern
    :param value: Value
    :return: bool
    """
    if (pattern is None) | (value is None):
        return False

    pat = re.fullmatch(str(pattern), str(value))

    if pat:
        return True
    else:
        return False


def count(pattern, value):
    """
    Returns the number of occurrences of given pattern
    :param pattern: Regex pattern
    :param value: value
    :return: int
    """

    result = re.findall(str(pattern), str(value))
    return len(result)

