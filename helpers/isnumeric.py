#!/usr/bin/env python3

import helpers.isexpectedformat as isexpectedformat

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


# Use this function to determine whether the given value is a number or not.
# Returns True if number, False otherwise.
# This returns True irrespective of whether the number is signed or unsigned.

def is_numeric(value):

    try:
        if isexpectedformat.validate(value=str(value), pattern=str("^-?\d+\.?\d*$")):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is a number or not.
# Returns True if number, False otherwise.
# This returns True only if the number is unsigned.

def is_unsigned_numeric(value):

    try:
        if isexpectedformat.validate(value=str(value), pattern=str("^\d+\.?\d*$")):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is a number or not.
# Returns True if number, False otherwise.
# This returns True only if the number is signed.

def is_signed_numeric(value):

    try:
        if isexpectedformat.validate(value=str(value), pattern=str("^-\d+\.?\d*$")):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is integer or not.
# Returns True if integer, False otherwise.
# This returns True irrespective of whether the integer is signed or unsigned

def is_integer(value):

    try:
        if isexpectedformat.validate(value=str(value), pattern=str("^-?\d+$")):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is integer or not.
# Returns True if integer, False otherwise.
# This returns True only if the integer is unsigned.

def is_unsigned_integer(value):

    try:
        if isexpectedformat.validate(value=str(value), pattern=str("^\d+$")):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except TypeError as tErr:
        raise TypeError(
            "The given value " + str(value) + " cannot be converted to string. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is integer or not.
# Returns True if integer, False otherwise.
# This returns True only if the integer is signed.

def is_signed_integer(value):

    try:
        if (isexpectedformat.validate(value=str(value), pattern=str("^-\d+$"))) | ("0" == str(value)):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except TypeError as tErr:
        raise TypeError(
            "The given value " + str(value) + " cannot be converted to string. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is decimal or not.
# Returns True if decimal, False otherwise.
# This returns True irrespective of whether the decimal is signed or unsigned

def is_decimal(value):

    try:
        if (isexpectedformat.validate(value=str(value), pattern=str("^-?\d+\.\d+$"))) | ("0" == str(value)):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is decimal or not.
# Returns True if decimal, False otherwise.
# This returns True only if the decimal is unsigned.

def is_unsigned_decimal(value):

    try:
        if (isexpectedformat.validate(value=str(value), pattern=str("\d+\.\d+$"))) | ("0" == str(value)):
            return True
        else:
            return False

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " is invalid. Exception : " + str(vErr))

    except TypeError as tErr:
        raise TypeError(
            "The given value " + str(value) + " cannot be converted to string. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))


# Use this function to determine whether the given value is decimal or not.
# Returns True if decimal, False otherwise.
# This returns True only if the decimal is signed.

def is_signed_decimal(value):

    try:
        if (isexpectedformat.validate(value=str(value), pattern=str("^-\d+\.\d+$"))) | ("0" == str(value)):
            return True
        else:
            return False

    except ValueError:
        # raise ValueError("The given value " +str(value)+ " is invalid. Exception : " + str(vErr))
        return False

    except TypeError as tErr:
        raise TypeError(
            "The given value " + str(value) + " cannot be converted to string. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))
