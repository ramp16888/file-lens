#!/usr/bin/env python3

# This module returns True if the given value is alphanumeric and False otherwise.

import helpers.isexpectedformat as isexpectedformat

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


def validate(value):

    try:

        isalphanumeric = isexpectedformat.validate(str(value), '[\w]+')
        return isalphanumeric

    except ValueError as vErr:
        raise ValueError("The given value " + str(value) + " could not be identified. Exception : " + str(vErr))

    except TypeError as tErr:
        raise TypeError(
            "The given value " + str(value) + " cannot be converted to string. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))
