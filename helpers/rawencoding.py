#!/usr/bin/env python3

# This module determines the type of encoding available in a file in its raw state (byte object).

import os
from .chardet import chardetect

__author__ = "Ram Praksh Jayapalan"
__email__ = "ramp16888@gmail.com"
__copyright__ = "Copyright 2017, crazy-brains project"
__maintainer__ = "Ram Prakash Jayapalan"
__lastupdated__ = "Jun 26, 2017"
__pyversion__ = "3.6.x"


def getfromfile(filepath):

    # Check whether the given file is a valid file path. If the filepath is not valid, raise FileNotFoundError
    # exception and quit

    if not os.path.isfile(filepath):
        raise FileNotFoundError("The given file path " + str(filepath) + " is not a valid file path.")

    # Now we use Chardet py package to help us determine the encoding of the file
    try:
        # Open the file in binary mode
        with open(str(filepath), "rb") as f:
            # Read 5000 bytes of data from the file no matter how large the file is and is used for sampling
            line = f.read(50000)
        result = chardetect.detect(line)
        charenc = result["encoding"]
        return charenc

    except ValueError as vErr:
        raise ValueError("The given file " + str(filepath) + " could not be read. Exception : " + str(vErr))

    except TypeError as tErr:
        raise TypeError(
            "The given filepath " + str(filepath) + " cannot be converted to string. Exception : " + str(tErr))

    except Exception as ex:
        raise Exception(str(ex))
